This presentation is written in Markdown for use with [Backslide](https://github.com/sinedied/backslide) (which finesses [remark.js](https://github.com/gnab/remark)).

The source is [presentation.md](presentation.md), which is processed into [dist/presentation.html](dist/presentation.html), which you'll need to download if you want to look at it.
