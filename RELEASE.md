# topplot

<!-- see https://github.com/openhwgroup/cva6/blob/master/CONTRIBUTING.md for icon suggestions -->

## Release info

### 0.2.2
#### Bugs fixed
:bug: #51 topplot crashes on Debian Testing (freezing for Debian 12)

### 0.2.1
#### Bugs fixed
:bug: #48 Handle threadnames from BusyBox output

### 0.2.0 #### New features

:sparkles: BusyBox top log files are now handled. Topplot started out just
           handling procps's top output format, but BusyBox top does its own
           thing, and now topplot handles either one. Huzzah! (closes issue #4)

### Other news
:clipboard: The parsing and graphing code (what else is there? :smile:) has
            been refactored. Mainly this means splitting out code from overly
            large files, so code's purpose is more clearly delineated. This
            should make maintenance and development more tractable.

### 0.1.9
:clipboard: Windows users with Python 3.9.9 and above may not have access to the keyboard controls.

            This is due to Matplotlib, a module used by Topplot, not (yet)
            adapting to changes in Python's Tk library.

            More details, including instructions for the simple changes needed to resolve the
            issue immediately, are to be found in [this bug report comment](https://gitlab.com/eBardie/topplot/-/issues/46#note_813754729).

#### Bugs fixed
:bug: #45 Topplot is now compatible with Matplotlib 3.5.0
:bug: #16 Legend interaction has been generally improved, but not quite in the
          way this ticket suggested.

          For multi-core figures, actions apply across all graphs.  Left and
          right clicking on the CPU Data legends is now consistent with
          clicking on other legends.  Clicking on legend lines/markers/labels
          should all work.

          The CPU overview graph's legend now has toggling legend lines like
          the other legends, that match the visiblity of the coresponding data
          lines.  Left and right clicking the legend title now works too.

### 0.1.8
:memo: And once more with passion. Images need raw links, text files look better as blobs :)

### 0.1.7
:memo: Absolutise missed link in README.md so that it works on the Topplot PyPi page.

### 0.1.6
:memo: Absolutise image links in README.md so that PyPi page has images.

### 0.1.5
#### Bugs fixed
:bug: #41 Displaying different graph windows was acting strangely on some platforms.

:bug: #40 The help info as a single page was too long for smaller screen sizes.
          Repeatedly pressing 'h' now cycles through shorter pages.

:bug: #39 Handle requests for per core graphs when there are fewer than three
      cores.

### 0.1.4
#### Bugs fixed

:bug: Handle requests for per core graphs when there's no per core data.

:bug: Handle the case when a CPU core happens to have no POI data.

### 0.1.3

:clipboard: The Makefile now has simple testing. It checks that topplot runs
            properly when asked to produce PNG files for the Overview figure
            for any files following this pattern logs/top.*.log. No validation
            is performed other than topplot's exit status.

#### Bugs fixed

:bug: Log files with summary CPU info in the header, rather than split out by
      core, were not producing a required temporary POI data file. This was a
      refactoring brainfart. It also highlights the need for testing.

### 0.1.2

:clipboard: The Python package was called 'topplot_' to facilitate the wrapper
            script being called 'topplot', but now the script is auto-generated
            by setuptools, and the Python package is called 'topplot'.

#### Bugs fixed

:bug: Logo now accessible when installed from .whl (e.g. via pip)

### 0.1.0

#### New features

:sparkles: The '--graph' option now accepts multiple characters from
           [12340cCpPqQ], so you can specify more than one figure.
           The specifiers [pPqQ] are new. 'p' saves the most recently opened
           figure as a PNG file, 'P' saves all the open figures as PNG files.
           Similarly 'q' closes the most recently opened figure, 'Q' closes
           all the open figures.

:sparkles: To give reassurance of activity, especially for larger log files or
		   more complex plots, there's now a progress window at startup and
		   when loading new figures.

:sparkles: To improve clarity, only the grouped menu is displayed on the
		   overview figure's cpu data's graph. It now also has load average
		   line.

:clipboard: If [Yappi](https://github.com/sumerc/yappi) is detected, the
		   `--profile` CLA becomes available. If this is option is passed to
		   topplot, both callgrind format profiling data and Yappi func stats
		   files are emitted.

#### Bugs fixed

:bug: Displaying core markers on the lines of the CPU graph is slow,
	  so now never happens on the overview window, and only on the separate CPU
	  figure if the commandline contains the `--display-cpu-markers` flag.

### 0.0.5

#### New features

:sparkles: Run on Windows as well as Linux. May run on other platforms too, but I've
           no way to test this. Please report results on other platforms.

:sparkles: Handle 'Threads' column when `top` is run with `-H`.

:sparkles: Handle explicit '[KMGTPE]iB' for mem and swap scales. Warns if it
           looks like the scale has been set too high.

:memo: Add documentation on how to configure **top** to output the data for various
       purposes, and in the correct format.

:clipboard: Add `--tmpfile DIR` CLA to enable inspection of temporary files
            which are otherwise automatically cleared away.

:clipboard: Add `--nowrite` CLA to disable writing to temporary files. Works
		    with `--tmpfile DIR` to allow interfering with test data.

#### Bugs fixed

:bug: Despite being written to output utf-8, `top` will spit out whatever /proc
      gives as commands. Now handling as ISO-8859-1. Not good for unicode, but
      an improvement on crashing.

:bug: Final process data was being skipped. Especially tricky when a filter
      reduces to a single process. No longer goes **BANG**!! or skips process.

:bug: CPU Steal data was only partially implemented. Now works if
      '--with-cpu-steal' CLA is given.

:bug: Issue https://gitlab.com/eBardie/topplot/-/issues/9 Displaying per core CPU info no longer flakey.

:bug: Issue https://gitlab.com/eBardie/topplot/-/issues/17 Toggling menus on POI per CPU core now works as expected.

<!-- vi: sw=2:ts=2:et:autoindent -->
