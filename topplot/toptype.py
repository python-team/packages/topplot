from enum import Enum


class TopType(Enum):
    PROCPS = 1
    BUSYBOX = 2
